import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import {
  AboutPage,
  BlogDetailPage,
  BlogPage,
  CarDetailPage,
  CarListingPage,
  ContactPage,
  HomePage,
  NotFoundPage
} from 'pages';

const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/home" />} />
      <Route path="/home" element={<HomePage />} />
      <Route path="/about" element={<AboutPage />} />
      <Route path="/cars" element={<CarListingPage />} />
      <Route path="/cars/:slug" element={<CarDetailPage />} />
      <Route path="/blogs" element={<BlogPage />} />
      <Route path="/blogs/:slug" element={<BlogDetailPage />} />
      <Route path="/contact" element={<ContactPage />} />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};

export default Routers;
