import React from 'react';
import { Container, Row } from 'reactstrap';
import { BlogListComponent, CommonComponent, HelmetComponent } from 'components';

const Blog = () => {
  return (
    <HelmetComponent title="Blogs">
      <CommonComponent title="Blogs" />
      <section>
        <Container>
          <Row>
            <BlogListComponent />
          </Row>
        </Container>
      </section>
    </HelmetComponent>
  );
};

export default Blog;
