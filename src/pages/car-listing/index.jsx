import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import { CarItemComponent, CommonComponent, HelmetComponent } from 'components';
import carData from 'assets/data/carData';

const CarListing = () => {
  return (
    <HelmetComponent title="Cars">
      <CommonComponent title="Car Listing" />

      <section>
        <Container>
          <Row>
            <Col lg="12">
              <div className=" d-flex align-items-center gap-3 mb-5">
                <span className=" d-flex align-items-center gap-2">
                  <i className="ri-sort-asc" /> Sort By
                </span>

                <select>
                  <option>Select</option>
                  <option value="low">Low to High</option>
                  <option value="high">High to Low</option>
                </select>
              </div>
            </Col>

            {carData.map((item) => (
              <CarItemComponent item={item} key={item.id} />
            ))}
          </Row>
        </Container>
      </section>
    </HelmetComponent>
  );
};

export default CarListing;
