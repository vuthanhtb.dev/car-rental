export { default as AboutPage } from './about';
export { default as BlogPage } from './blog';
export { default as BlogDetailPage } from './blog-detail';
export { default as CarDetailPage } from './car-detail';
export { default as CarListingPage } from './car-listing';
export { default as ContactPage } from './contact';
export { default as HomePage } from './home';
export { default as NotFoundPage } from './not-found';
