import React from 'react';
import { Link } from 'react-router-dom';
import { HelmetComponent } from 'components';

const NotFound = () => {
  return (
    <HelmetComponent title="Not Found">
      <div className="d-flex align-items-center justify-content-center not-found">
        <div className="text-center">
            <h1 className="display-1 fw-bold">404</h1>
            <p className="fs-3">
              <span className="text-danger">Opps!</span> Page not found.
            </p>
            <p className="lead">The page you're looking for doesn't exist.</p>
            <Link to={"/home"} className="btn btn-primary">Go Home</Link>
        </div>
      </div>
    </HelmetComponent>
  );
};

export default NotFound;
