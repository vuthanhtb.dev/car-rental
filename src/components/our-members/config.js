import ava01 from 'assets/all-images/ava-1.jpg';
import ava02 from 'assets/all-images/ava-2.jpg';
import ava03 from 'assets/all-images/ava-3.jpg';

export const OUR_MEMBERS = [
  {
    name: 'John Doe',
    experience: '5 years of experience',
    fbUrl: '#',
    instUrl: '#',
    twitUrl: '#',
    linkedinUrl: '#',
    imgUrl: ava01,
  },

  {
    name: 'David Lisa',
    experience: '5 years of experience',
    fbUrl: '#',
    instUrl: '#',
    twitUrl: '#',
    linkedinUrl: '#',
    imgUrl: ava02,
  },

  {
    name: 'Hilton King',
    experience: '5 years of experience',
    fbUrl: '#',
    instUrl: '#',
    twitUrl: '#',
    linkedinUrl: '#',
    imgUrl: ava03,
  },

  {
    name: 'John Doe',
    experience: '5 years of experience',
    fbUrl: '#',
    instUrl: '#',
    twitUrl: '#',
    linkedinUrl: '#',
    imgUrl: ava01,
  },
];
