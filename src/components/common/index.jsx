import React from 'react';
import { Container } from 'reactstrap';

const Common = (props) => {
  return (
    <section className="common__section mb-5">
      <Container className="text-center">
        <h1 className="text-light">{props.title}</h1>
      </Container>
    </section>
  );
};

export default Common;
