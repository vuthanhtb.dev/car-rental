export const quickLinks = [
  {
    path: '/about',
    display: 'About',
  },
  {
    path: '#',
    display: 'Privacy Policy',
  },
  {
    path: '/cars',
    display: 'Car Listing',
  },
  {
    path: '/blogs',
    display: 'Blog',
  },
  {
    path: '/contact',
    display: 'Contact',
  },
];
