import React from 'react';

const Helmet = (props) => {
  const { title, children } = props;
  document.title = `Rent Car Service - ${title}`;

  return (
    <div className="w-100">
      {children}
    </div>
  );
};

export default Helmet;
