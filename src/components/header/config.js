export const navLinks = [
  {
    path: '/home',
    display: 'Home',
  },
  {
    path: '/about',
    display: 'About',
  },
  {
    path: '/cars',
    display: 'Cars',
  },

  {
    path: '/blogs',
    display: 'Blog',
  },
  {
    path: '/contact',
    display: 'Contact',
  },
];
