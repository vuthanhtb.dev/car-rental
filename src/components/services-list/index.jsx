import React from 'react';
import ServiceItem from './serviceItem';
import serviceData from 'assets/data/serviceData';

const ServicesList = () => {
  return (
    <>
      {serviceData.map((item) => (
        <ServiceItem item={item} key={item.id} />
      ))}
    </>
  );
};

export default ServicesList;
