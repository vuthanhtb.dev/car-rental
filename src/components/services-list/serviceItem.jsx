import React from 'react';
import { Col } from 'reactstrap';

const ServiceItem = (props) => {
  const { icon, title, desc } = props.item;
  return (
    <Col lg="4" md="4" sm="6" className="mb-3">
    <div className="service__item">
      <span className="mb-3 d-inline-block">
        <i className={icon} />
      </span>

      <h6>{title}</h6>
      <p className="section__description">{desc}</p>
    </div>
  </Col>
  );
};

export default ServiceItem;
