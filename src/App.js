import React from 'react';
import Routers from 'routes';
import { FooterComponent, HeaderComponent } from 'components';

import 'styles/header.css';
import 'styles/common.css';
import 'styles/about.css';
import 'styles/hero-slider.css';
import 'styles/about-component.css';
import 'styles/become-driver.css';
import 'styles/our-member.css';
import 'styles/blog-item.css';
import 'styles/blog-details.css';
import 'styles/car-item.css';
import 'styles/booking-form.css';
import 'styles/testimonial.css';
import 'styles/find-car-form.css';
import 'styles/payment-method.css';
import 'styles/contact.css';
import 'styles/services-list.css';
import 'styles/not-found.css';
import 'styles/footer.css';

const App = () => {
  return (
    <React.Fragment>
      <HeaderComponent />
      <div>
        <Routers />
      </div>
      <FooterComponent />
    </React.Fragment>
  );
};

export default App;
